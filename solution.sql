


-- 1)
SELECT customerName AS Customer, country FROM customers WHERE country = "Philippines" ORDER BY country DESC;

-- 2)
SELECT contactFirstName AS ContactFirstName, contactLastName AS ContactLastName, customerName AS Customer FROM customers WHERE customerName = "La Rochelle Gifts";

-- 3)
SELECT productName AS Product, MSRP FROM products WHERE productName = "The Titanic";

-- 4)
SELECT firstName AS FirstName, lastName AS LastName FROM employees WHERE email = "jfirrelli@classicmodelcars.com"; 

-- 5)
SELECT customerName AS Customer FROM customers WHERE state IS NULL;

-- 6)
SELECT firstName AS FirstName, lastName AS LastName, email FROM employees WHERE lastName = "Patterson" AND firstName = "Steve";

-- 7)
SELECT customerName AS Name, country, creditLimit AS Credit FROM customers WHERE country != "USA" AND creditLimit > 3000 ORDER BY creditLimit ASC;

-- 8)
SELECT customerNumber AS Customer FROM orders WHERE comments LIKE "%DHL%";

-- 9)
SELECT * FROM productlines WHERE textDescription LIKE "%state of the art%";

-- 10)
SELECT DISTINCT country FROM customers ORDER BY country ASC;

-- 11)
SELECT DISTINCT status FROM orders ORDER BY status ASC;

-- 12)
SELECT customerName AS Name, country FROM customers WHERE country = "USA" OR country = "France" OR country = "Canada" ORDER BY country ASC;

-- 13)
SELECT firstName AS FirstName, lastName AS LastName, city FROM employees
	JOIN offices ON employees.officeCode = offices.officeCode WHERE city = "Tokyo" ORDER BY lastName ASC;

-- 14)
SELECT customerName AS Name FROM customers WHERE salesRepEmployeeNumber = 1166 ORDER BY customerName ASC;

SELECT customerName AS Name FROM customers 
	JOIN employees ON customers.salesRepEmployeeNumber = employees.employeeNumber
	WHERE firstName = "Leslie" AND lastName = "Thompson" ORDER BY customerName ASC;

-- 15)
SELECT customers.customerName AS Name, products.productName AS Product FROM customers
	JOIN orders ON customers.customerNumber = orders.customerNumber
	JOIN orderdetails ON orders.orderNumber = orderdetails.orderNumber
	JOIN products ON orderdetails.productCode = products.productCode
	WHERE customerName = "Baane Mini Imports"
	ORDER BY productName ASC;

-- 16)
SELECT employees.firstName, employees.lastName, customers.customerName AS customer, customers.country AS customerCountry, offices.country AS officeCountry FROM customers
	JOIN employees ON customers.salesRepEmployeeNumber = employees.employeeNumber
	JOIN offices ON employees.officeCode = offices.officeCode
	WHERE customers.country = offices.country
	ORDER BY customerName ASC;

-- 17)
SELECT products.productName AS Product, products.quantityInStock AS Stock FROM products
	WHERE productLine = "planes" AND quantityInStock < 1000;

-- 18)
SELECT customerName AS Customer, phone FROM customers
	WHERE phone LIKE "+81%"
	ORDER BY customerName ASC;
